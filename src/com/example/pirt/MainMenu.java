package com.example.pirt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.os.Build;

public class MainMenu extends ActionBarActivity {
	
	BluetoothAdapter bluetooth;
	protected static final String TAG = "BLUETOOTH";
    protected static final int DISCOVERY_REQUEST = 1;
    private BluetoothSocket btSocket = null;
    private OutputStream outstream = null;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_menu);
		
	
		final BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
		this.bluetooth=bluetooth;		
		if (bluetooth.isEnabled()) {
			String address = bluetooth.getAddress();
			String name = bluetooth.getName();
			}
		else
		{
			startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), 0);
			String address = bluetooth.getAddress();
			String name = bluetooth.getName();
		}
		
	
		
		
		final Button Scanbutton = (Button) findViewById(R.id.scanbutton);
		final Button ClearButton = (Button) findViewById(R.id.clearbutton);
		final EditText ScanResults = (EditText) findViewById(R.id.scanresults);
		ClearButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				ScanResults.setText("");
			}});
		
		
        Scanbutton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
              // startDiscovery(); 
               String laptopentry;
               String PIEntry;
               laptopentry= "00:24:7E:74:EE:C9";
               PIEntry ="00:16:A4:06:BD:61";
               BluetoothDevice device =null;
               UUID uuid = null;
               
               try
               {
            	   		  device =bluetooth.getRemoteDevice(laptopentry);
            	   		  uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            		      btSocket = device.createRfcommSocketToServiceRecord(uuid);
            		      bluetooth.cancelDiscovery();
            		      btSocket.connect();
            		      ScanResults.setText("scanning");
            		      System.out.println("test");
            		      String message = "testtext\n";
            		      byte[] msgBuffer = message.getBytes();
            		      outstream = btSocket.getOutputStream();
            		      outstream.write(msgBuffer);
            		      InputStream inStream = btSocket.getInputStream();
            		      BufferedReader bReader=new BufferedReader(new InputStreamReader(inStream));
            		     // String response=bReader.readLine();
            		      String response="";
            		      response = ReadIn(inStream, bReader);
            		      ScanResults.setText(response);
            		      System.out.println(response);
            		    } catch (IOException e) {
            		      Log.d(TAG, " failed to create socket", e);
            		    }
            }
        });
        
      
	}
	
	private String ReadIn(InputStream instream, BufferedReader reader)
	{
		//InputStream instream = null;
	//	try 
	//	{
	//		InputStream inStream = socket.getInputStream();
	//	} 
	//	catch (IOException e) {
			// TODO Auto-generated catch block
	//		e.printStackTrace();
	//		return "didnt work";
	//	}String result = "";
		String r;
		String results="";
		
		
		try {
			
			
			while(!(r=reader.readLine()).contains("endresults")){System.out.println("BLAH: "+r);
			results=(results+"\n"+r);
			}
			
			return results;
			/*
			String response = reader.readLine();
			if(response!="end/n")
			{	
				System.out.println("got here ");
				System.out.println("got here " +results);
				results=(results+"/n"+response);
				ReadIn(instream, reader, results);
			}
			else
			{
				return results;
			}*/
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return"scan failed";
		}
		//return results;
	}
	
    private ArrayList<BluetoothDevice> deviceList = new ArrayList<BluetoothDevice>();
	
	 private void startDiscovery() {
	      registerReceiver(discoveryResult, new IntentFilter(BluetoothDevice.ACTION_FOUND));

	      if (bluetooth.isEnabled() && !bluetooth.isDiscovering())
	        deviceList.clear();
	        bluetooth.startDiscovery();
	    }
	 
	 BroadcastReceiver discoveryResult = new BroadcastReceiver() {
	      @Override
	      public void onReceive(Context context, Intent intent) {
	        String remoteDeviceName = 
	          intent.getStringExtra(BluetoothDevice.EXTRA_NAME);

	        BluetoothDevice remoteDevice =  
	          intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

	        deviceList.add(remoteDevice);

	        Log.d(TAG, "Discovered " + remoteDeviceName);
	      }
	    };
	    private BluetoothSocket transferSocket;
	    
	    private void connectToServerSocket(BluetoothDevice device, UUID uuid) {
	        try{
	          BluetoothSocket clientSocket 
	            = device.createRfcommSocketToServiceRecord(uuid);

	          // Block until server connection accepted.
	          clientSocket.connect();

	          // Start listening for messages.
	          StringBuilder incoming = new StringBuilder();
	          listenForMessages(clientSocket, incoming);

	          // Add a reference to the socket used to send messages.
	          transferSocket = clientSocket;

	        } catch (IOException e) {
	          Log.e("BLUETOOTH", "Blueooth client I/O Exception", e);
	        }
	      }
	    
	    
	    private boolean listening = false;
	    
	    
	    
	    private void listenForMessages(BluetoothSocket socket, StringBuilder incoming) 
	    {
	    	listening = true;
	    	int bufferSize = 1024;
	    	byte[] buffer = new byte[bufferSize];

	    	try 
	    	{
	    		InputStream instream = socket.getInputStream();
	    		int bytesRead = -1;

	    		while (listening) 
	    		{
	    			bytesRead = instream.read(buffer);
	    			if (bytesRead != -1) {
	    				String result = "";
	    				while ((bytesRead == bufferSize) &&
	    						(buffer[bufferSize-1] != 0))
	    				{
	    					result = result + new String(buffer, 0, bytesRead - 1);
	    					bytesRead = instream.read(buffer);
	    				}
	    				result = result + new String(buffer, 0, bytesRead - 1);
	    				incoming.append(result);
	    			}
	    			socket.close();
	    		}
	    	} catch (IOException e) {
	    		Log.e(TAG, "Message received failed.", e);
	    	}
	    	finally 
	    	{
	    	}
	    }
  
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}



}
